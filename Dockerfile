FROM golang:1.13

WORKDIR /usr/src

RUN mkdir upload
RUN mkdir download

RUN go get -u "github.com/joho/godotenv"
RUN go get -u "github.com/aws/aws-sdk-go"

COPY . .

EXPOSE 9050

CMD ["go", "run", "./src/main.go"]
