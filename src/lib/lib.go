package lib

import (
  "encoding/json"
  "fmt"
  "io/ioutil"
  "math/rand"
  "os"
)

const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

func PrintMessage(m string) {
  fmt.Println(m)
}

func LogError(err error) {
  if err != nil {
    fmt.Println(err)
    return
  }
}

func ErrorPanic(err error) {
  if err != nil {
    panic(err)
  }
}

func ReturnStatus(s string) []byte {
  r := map[string]interface{}{"status": s}
  data, err := json.Marshal(r)
  LogError(err)

  return data
}

func GetEnv(key, fallback string) string {
  if value, ok := os.LookupEnv(key); ok {
    return value
  }
  return fallback
}

func GetRootDir() string {
  dir, err := os.Getwd()
  LogError(err)
  return dir
}

func GetFullPath(directory string, name string) string {
  root := GetRootDir()
  path := root + "/" + directory + "/" + name
  return path
}

func ReadFileString(directory string, name string) {
  path := GetFullPath(directory, name)
  f, _ := ioutil.ReadFile(path)
  s := string(f)
  fmt.Println(s)
}

func MakeRandomString(n int) string {
  b := make([]byte, n)

  for i := range b {
    b[i] = characters[rand.Int63() % int64(len(characters))]
  }
  return string(b)
}
