package main

import (
  "./config"
  "./handlers"
  "./lib"

  "fmt"
  "log"
  "net/http"
)

func main() {
  // Routes
  http.HandleFunc("/api/upload", handlers.UploadHandler)
  http.HandleFunc("/api/download", handlers.DownloadHandler)
  http.HandleFunc("/api/delete", handlers.DeleteHandler)

  p := config.APP_CONFIG.Port
  ps := fmt.Sprintf(":%d", p)
  env := lib.GetEnv("APP_ENV", "development")
  i := fmt.Sprintf("Starting %s server on port %d", env, p)
  lib.PrintMessage(i)

  log.Fatal(http.ListenAndServe(ps, nil))
}
