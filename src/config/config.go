package config

import (
  "../lib"

  "log"
  "os"

  "github.com/joho/godotenv"
)

type (
  App struct {
    Name string
    Port uint
  }
)

var (
  APP_CONFIG *App

  APP_ENV string
  AWS_REGION_NAME string
  AWS_SECRET_ID string
  AWS_SECRET_KEY string
  AWS_BUCKET_NAME string
)

func init() {
  err := godotenv.Load()
  if err != nil {
    log.Fatal("Error loading .env file")
  }

  initConfig()
}


func initConfig() {
  APP_CONFIG = &App {
    Name: "Go Files",
    Port: 9050,
  }

  APP_ENV = lib.GetEnv("APP_ENV", "development")

  AWS_REGION_NAME = os.Getenv("AWS_REGION_NAME")
  AWS_SECRET_ID = os.Getenv("AWS_SECRET_ID")
  AWS_SECRET_KEY = os.Getenv("AWS_SECRET_KEY")
  AWS_BUCKET_NAME = os.Getenv("AWS_BUCKET_NAME")
}
