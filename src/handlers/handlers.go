package handlers

import (
  "../lib"
  "../operations"

  "net/http"
)

func UploadHandler(w http.ResponseWriter, r *http.Request) {
  filename := "text.txt"

  status := operations.UploadFileToS3(filename)
  res := lib.ReturnStatus(status)
  w.Write([]byte(res))
}

func DownloadHandler(w http.ResponseWriter, r *http.Request) {
  filename := "onrHFkSuohOf8I.txt"

  status := operations.DownloadFileFromS3(filename)
  res := lib.ReturnStatus(status)
  w.Write([]byte(res))
}

func DeleteHandler(w http.ResponseWriter, r *http.Request) {
  filename := "onrHFkSuohOf8I.txt"

  status := operations.DeleteFileFromS3(filename)
  res := lib.ReturnStatus(status)
  w.Write([]byte(res))
}
