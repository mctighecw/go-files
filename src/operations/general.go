package operations

import (
  "../config"
  "../lib"

  "github.com/aws/aws-sdk-go/aws"
  "github.com/aws/aws-sdk-go/aws/credentials"
  "github.com/aws/aws-sdk-go/aws/session"
  "github.com/aws/aws-sdk-go/service/s3"
)

func createSession() (*session.Session) {
  AWS_REGION_NAME := config.AWS_REGION_NAME
  AWS_SECRET_ID := config.AWS_SECRET_ID
  AWS_SECRET_KEY := config.AWS_SECRET_KEY

  sess, err := session.NewSession(&aws.Config{
      Region: aws.String(AWS_REGION_NAME),
      Credentials: credentials.NewStaticCredentials(
        AWS_SECRET_ID,
        AWS_SECRET_KEY,
        "",
      ),
    },
  )

  if err != nil {
    lib.PrintMessage("Could not create AWS session")
    lib.LogError(err)
    return nil
  } else {
    return sess
  }
}

func CreateS3Session() *s3.S3 {
  AWS_REGION_NAME := config.AWS_REGION_NAME
  AWS_SECRET_ID := config.AWS_SECRET_ID
  AWS_SECRET_KEY := config.AWS_SECRET_KEY

  sess := s3.New(session.New(), &aws.Config{
    Region: aws.String(AWS_REGION_NAME),
    Credentials: credentials.NewStaticCredentials(
      AWS_SECRET_ID,
      AWS_SECRET_KEY,
      "",
    ),
  })

  return sess
}
