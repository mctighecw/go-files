package operations

import (
  "../config"
  "../lib"

  "fmt"

  "github.com/aws/aws-sdk-go/aws"
  "github.com/aws/aws-sdk-go/service/s3"
)

func DeleteFileFromS3(filename string) string {
  AWS_BUCKET_NAME := config.AWS_BUCKET_NAME

  sess := CreateS3Session()

  input := &s3.DeleteObjectInput{
    Bucket: aws.String(AWS_BUCKET_NAME),
    Key: aws.String(filename),
  }

  _, err := sess.DeleteObject(input)

  var status string
  if err != nil {
    lib.LogError(err)
    status = fmt.Sprintf("Error deleting file %s", filename)
  } else {
    status = fmt.Sprintf("File %s deleted successfully", filename)
  }
  return status
}
