package operations

import (
  "../config"
  "../lib"

  "fmt"
  "os"
  "path/filepath"

  "github.com/aws/aws-sdk-go/aws"
  "github.com/aws/aws-sdk-go/service/s3/s3manager"
)

func UploadFileToS3(filename string) string {
  AWS_BUCKET_NAME := config.AWS_BUCKET_NAME

  sess := createSession()
  path := lib.GetFullPath("upload", filename)

  file, err1  := os.Open(path)
  if err1 != nil {
    lib.LogError(err1)
  }
  defer file.Close()

  nameHash := lib.MakeRandomString(14)
  extension := filepath.Ext(filename)
  newFilename := nameHash + extension

  uploader := s3manager.NewUploader(sess)

  _, err2 := uploader.Upload(&s3manager.UploadInput{
    Bucket: aws.String(AWS_BUCKET_NAME),
    Key: aws.String(newFilename),
    Body: file,
  })

  var status string
  if err2 != nil {
    lib.LogError(err2)
    status = fmt.Sprintf("Error uploading file %s", filename)
  } else {
    status = fmt.Sprintf("Uploaded file %s as %s", filename, newFilename)
  }
  return status
}
