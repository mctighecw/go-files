package operations

import (
  "../config"
  "../lib"

  "fmt"
  "os"

  "github.com/aws/aws-sdk-go/aws"
  "github.com/aws/aws-sdk-go/service/s3"
  "github.com/aws/aws-sdk-go/service/s3/s3manager"
)

func DownloadFileFromS3(filename string) string {
  AWS_BUCKET_NAME := config.AWS_BUCKET_NAME

  sess := createSession()

  path := lib.GetFullPath("download", filename)
  file, err1 := os.Create(path)
  if err1 != nil {
    lib.LogError(err1)
  }
  defer file.Close()

  downloader := s3manager.NewDownloader(sess)

  numBytes, err2 := downloader.Download(file,
    &s3.GetObjectInput{
      Bucket: aws.String(AWS_BUCKET_NAME),
      Key: aws.String(filename),
    },
  )

  var status string
  if err2 != nil {
    os.Remove(path)
    lib.LogError(err2)
    status = fmt.Sprintf("Error downloading file %s", filename)
  } else {
    status = fmt.Sprintf("Downloaded file %s (%d bytes)", filename, numBytes)
  }
  return status
}
