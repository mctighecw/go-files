# README

A mini app to upload to, download from, and delete files from an AWS S3 secure bucket using Go (_Golang_).

## App Information

App Name: go-files

Created: February 2020

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/go-files)

## Tech Stack

- Go
- GoDotEnv
- AWS SDK for Go
- Docker

## To Run via Docker

1. Make AWS account, create S3 bucket, set permissions
2. Add .env file with necessary variables to project root
3. Run commands:

```
$ cd go-files
$ source build.sh
```

_Note_: Since this is a simple backend template without frontend user file upload functionality, the files to upload should be placed in the `upload` folder, then the name of the file to upload changed in the `UploadHandler`.

## Routes

- Upload file: `http://localhost:9050/api/upload`
- Download file: `http://localhost:9050/api/download`
- Delete file: `http://localhost:9050/api/delete`

Last updated: 2024-09-06
